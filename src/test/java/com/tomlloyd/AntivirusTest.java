package com.tomlloyd;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AntivirusTest {

    @Mock
    private S3Service s3Service;

    @InjectMocks
    private Antivirus antivirus;

    @Captor
    private ArgumentCaptor<String> fileCaptor;

    @Before
    public void setUp() {
        initMocks(this);
        mockS3();
    }

    @Test
    public void downloadSafeFilesFromS3_goodAndBadFile_filterAndDownload() {
        // GIVEN
        registerTestFiles("GOOD.txt", "BAD.txt");
        List<String> expectedFiles = Collections.singletonList("GOOD.txt");

        // WHEN
        antivirus.downloadSafeFilesFromS3("my-bucket", "my-key");

        // THEN
        verify(s3Service, times(1)).listFiles(anyString(), anyString());
        verify(s3Service, times(1)).downloadFile(anyString(), fileCaptor.capture());
        assertThat(fileCaptor.getAllValues(), is(expectedFiles));
    }

    @Test
    public void downloadSafeFilesFromS3_badFileOnly_noDownload() {
        // GIVEN
        registerTestFiles("BAD.txt");

        // WHEN
        antivirus.downloadSafeFilesFromS3("my-bucket", "my-key");

        // THEN
        verify(s3Service, times(1)).listFiles(anyString(), anyString());
        verify(s3Service, never()).downloadFile(anyString(), anyString());
    }

    private void mockS3() {
        doNothing().when(s3Service).downloadFile(anyString(), anyString());
    }

    private void registerTestFiles(String... files) {
        when(s3Service.listFiles(anyString(), anyString())).thenReturn(Arrays.asList(files));
    }
}