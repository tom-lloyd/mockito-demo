package com.tomlloyd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class Antivirus {

    private static final Logger LOG = LoggerFactory.getLogger(Antivirus.class);

    @Inject
    private S3Service s3Service;

    public void downloadSafeFilesFromS3(String bucket, String key) {
        LOG.info(">> Searching for files at bucket='{}' key='{}' ...", bucket, key);

        final List<String> files = s3Service.listFiles(bucket, key);

        if (files.isEmpty()) {
            LOG.info("<< No files found");
            return;
        }

        LOG.info("-- Found files='{}'", files);

        filterFiles(bucket, files);
    }

    private void filterFiles(String bucket, List<String> files) {
        LOG.info("-- Filtering out bad files...");

        final List<String> processedFiles = files.stream()
                .filter(file -> !file.contains("BAD"))
                .collect(Collectors.toList());

        if (processedFiles.isEmpty()) {
            LOG.info("<< No files were downloaded");
            return;
        }

        LOG.info("-- Filtered files='{}'", processedFiles);

        downloadFiles(bucket, processedFiles);
    }

    private void downloadFiles(String bucket, List<String> files) {
        LOG.info("-- Downloading files...");

        files.forEach(file -> s3Service.downloadFile(bucket, file));

        LOG.info("<< Success! 😜");
    }
}
