package com.tomlloyd;

import java.util.List;

public interface S3Service {

    List<String> listFiles(String bucket, String key);

    void downloadFile(String bucket, String key);
}
